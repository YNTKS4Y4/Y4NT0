import { SCENES } from '../constants';
export default class GameScene extends Phaser.Scene {
  constructor() {
    super({
      key: SCENES.GAME,
    });
  }

  //////////////////////////////////////////////////
  // LIFECYCLE (init, preload, create, update)    //
  //////////////////////////////////////////////////

  init(): void {}

  preload(): void {}

  create(): void {
    this.add.text(350, 300, 'Hello World', {
      fontFamily: 'sans-serif',
      color: '#fff',
    });
    this.scene.launch(SCENES.UI);
  }

  update(): void {}

  //////////////////////////////////////////////////
  // Public methods                               //
  //////////////////////////////////////////////////

  //////////////////////////////////////////////////
  // Private methods                              //
  //////////////////////////////////////////////////
}
