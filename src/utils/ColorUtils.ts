/**
 * Color converters
 */
export default class ColorUtils {
  /**
   * Converts Phaser Color class to HTML compatible color string, prefixed with #
   * @param color Phaser Color class
   */
  static colorToString(color: Phaser.Display.Color): string {
    return Phaser.Display.Color.RGBToString(
      color.red,
      color.green,
      color.blue,
      color.alpha,
      '#',
    );
  }

  /**
   * Converts Phaser Color class to rgba color string rgba(r,g,b,a)
   * @param color Phaser Color class
   */
  static colorToRGBAString(color: Phaser.Display.Color): string {
    return `rgba(${color.red},${color.green},${color.blue},1)`;
  }

  /**
   * Converts Phaser Color class to hex number
   * @param color Phaser Color class
   */
  static colorToHex(color: Phaser.Display.Color): number {
    const hexString = `0x${Phaser.Display.Color.ComponentToHex(
      color.red,
    )}${Phaser.Display.Color.ComponentToHex(
      color.green,
    )}${Phaser.Display.Color.ComponentToHex(color.blue)}`;

    return Number(hexString);
  }
}
